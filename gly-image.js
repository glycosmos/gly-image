import { LitElement, html, customElement, property } from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';
import {getApiDomain} from 'gly-domain/gly-domain.js';

export class GtcWcImage extends LitElement {

  static get properties() {
    return {
      accession: {
        type: String
      },
      imagestyle: {
        type: String
      },
      format: {
        type: String
      },
      notation: {
        type: String
      },
      generation: {
        type: String
      },
      imagegraph: {
        type: String
      }
    };
  }

  render() {
    return html `
    <style>
    img {
      width: auto;
      height:auto;
      max-width:100%;
      vertical-align: middle;
      border:0;
    }
    </style>
    <div>
      <a href="https://glytoucan.org/Structures/Glycans/${this.accession}">${this._processHtml()}</a>
    <div>
 `;
  }

  constructor() {
    super();
    this.accession="G00029MO";
    this.imagestyle="extended";
    this.format="png";
    this.notation="snfg";
    this.generation="rdf";
    this.graph = 'http://rdf.glytoucan.org/image';
    this.flag = false;
  }

  connectedCallback() {
    super.connectedCallback();
    if(this.notation === "cfg" || this.notation === "cfgbw"){
      this.notation = "snfg";
    }else if(this.notation === "uoxf"){
      this.notation = "uoxf-color"
    }
    const url = 'https://image.glycosmos.org/' + this.notation + '/png/' + this.accession
    this.getUrl(url);
  }

  async getUrl(url) {
    await fetch(url,{
      mode: 'cors'
    }).then(function(response) {
      if(response.ok) {
        console.log("response ok");
      } else {
        throw new Error();
      }
    }).then(text=>{
        this.flag = true;
    });
    await this.requestUpdate();
  }

  _processHtml() {
    if (this.flag) {
      return html`<img src="https://image.glycosmos.org/${this.notation}/png/${this.accession}" />`;
    } else {
      const apihost =  getApiDomain(location.ref);
      return html`<img src="https://${apihost}/wurcs2image/latest/png/binary/${this.accession}" />`;

    }
  }

}
customElements.define('gly-image', GtcWcImage);
